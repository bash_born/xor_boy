#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int xor_enc(char *input_file, char *output_file, char *get_key);

int xor_dec(char *input_file, char *output_file, char *get_key);

int main(int argc, char *argv[]){
	
	char *banner = "\n"
"             _           \n"
" _ _ ___ ___| |_ ___ _ _ \n"
"|_'_| . |  _| . | . | | |\n"
"|_,_|___|_| |___|___|_  |\n"
"                    |___|\n\n"
"-----=[Version:0.1               ]=-\n"
"-----=[XOR boy                   ]=-\n"
"-----=[Creator: button-dependent ]=-\n";

	char *help_msg = "Usage:\n\n"
	"\t./xorboy or ./xorboy --help <- show this message\n\n"
	"Encryption:\n\n"
	"\t./xorboy -e <source file> <file name for encrypted output> <key>\n\n"
	"Decryption:\n\n"
	"\t./xorboy -d <encrypted file> <file name for output> <key>\n\n";

	if(argc < 2 || !(strcmp(argv[1], "--help")))

		puts(help_msg);

	else if(!(strcmp(argv[1], "-e"))){

		puts(banner);

		xor_enc(argv[2], argv[3], argv[4]);
	
	}else if(!(strcmp(argv[1], "-d"))){
		
		puts(banner);

		xor_dec(argv[2], argv[3], argv[4]);	
		
	}else

		puts(help_msg);

}


int xor_enc(char *input_file, char *output_file, char *get_key)
{
	int i, key_size = strlen(get_key);

	char key[key_size], enc_ch;

	strcpy(key, get_key);

	FILE *in_file, *out_file;

	if(!(in_file = fopen(input_file, "r"))){

		perror("xorboy");

		return -1;

	}

	if(!(out_file = fopen(output_file, "w"))){

		perror("xorboy");

		return -1;
	}

	printf("xorboy: starting encryption...\n");

	printf("xorboy: key -> [%s]\n", get_key);

	while(1){

		enc_ch = fgetc(in_file);

		if((feof(in_file))){

			printf("xorboy: encryption completed\n");

			break;

		}

		for(i = 0; i < key_size; i++)

			enc_ch ^= key[i];

		fprintf(out_file, "%c", enc_ch);

	}

	return 0;
	
}

int xor_dec(char *input_file, char *output_file, char *get_key)
{

	int i, key_size = strlen(get_key);

	char key[key_size], dec_ch;

	strcpy(key, get_key);

	FILE *in_file, *out_file;

	if(!(in_file = fopen(input_file, "r"))){

		perror("xorboy");

		return -1;

	}

	if(!(out_file = fopen(output_file, "w"))){

		perror("xorboy");

		return -1;
	}

	printf("xorboy: starting decryption...\n");

	printf("xorboy: key -> [%s]\n", get_key);

	while(1){

		dec_ch = fgetc(in_file);

		if((feof(in_file))){

			printf("xorboy: decryption completed\n");

			break;

		}

		for(i = 0; i < key_size; i++)

			dec_ch ^= key[i];

		fprintf(out_file, "%c", dec_ch);

	}

	return 0;

}
